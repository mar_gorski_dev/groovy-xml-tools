if (args.length != 1) {
    println "Use echo each line [fileName]"
    System.exit(1)
}

void writeToFile(String directory, String fileName, String extension, String fileContent) {
    new File("$directory/$fileName$extension").withWriter("UTF-8") { out ->
        out.println(fileContent)
    }
}


def lines = new File(args[0]).readLines()

lines.each { String line ->
    def fileName = UUID.randomUUID().toString()
    writeToFile("target", fileName, ".txt", line)
}
