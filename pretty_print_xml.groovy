import groovy.xml.*

if (args.length != 1) {
    println "Use echo each line [fileName]"
    System.exit(1)
}

def document = new File(args[0]).getText('UTF-8')

println XmlUtil.serialize(document)
