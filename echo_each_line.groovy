if (args.length != 1) {
    println "Use echo each line [fileName]"
    System.exit(1)
}

def lines = new File(args[0]).readLines()

lines.each { String line ->
    println line
}
